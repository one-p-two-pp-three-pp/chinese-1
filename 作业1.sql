create database studentinfo

go
use studentinfo

--drop table student 
--drop table exam
create table student(
	stuno int  primary key identity(2501,1) not null,
	stuname nvarchar(10),
	stuage int,
	stuaddress nvarchar(100),
	stuseat int,
	stusex int check(stusex=1 or stusex=0)
)
create table exam(
	exno int primary key identity(1,1) not null,
	stuno int foreign key(stuno) references student(stuno),
	exwriten int,
	exlab int 
)
insert into student(stuname, stuage, stuaddress, stuseat, stusex)
values('������' , '20' , '�������' , '1' ,'1'),
	  ('��˹��' , '18' , '�����人' , '2' ,'0'),
	  ('���Ĳ�' , '22' , '���ϳ�ɳ' , '3' ,'1'),
	  ('ŷ������' , '21' , '�����人' , '4' ,'0'),
	  ('÷����' , '20' , '�����人' , '5' ,'1'),
	  ('������' , '19' , '�������' , '6' ,'1'),
	  ('�·�' , '20' , '�������' , '7' ,'0')
insert into exam(stuno, exwriten, exlab)
values(2501 , 50  , 70 ),
(2502 ,  60 , 65 ),
(2503 ,  95 , 95 ),
(2504 ,  40 , 80 ),
(2505 ,  70 , 90 ),
(2506 ,  85 , 90 )
select * from student 
select * from exam
select stuno'���',stuname'����',stuage'����',stuaddress'��ַ',stuseat'��λ',stusex'�Ա�' from student
select stuname,stuage,stuaddress from student
select stuno'ѧ��',����=exwriten,exlab as '����' from exam
select stuname,stuage,stuaddress,stuname+'@'+stuaddress'����' from student
select stuno,exlab,exwriten,exlab+exwriten'�ܷ�' from exam 
select distinct stuaddress from student
select distinct stuage'����' from student
select top 3 stuno from student
select top 4 stuseat,stuname from student
select top 50 percent * from student
select * from student where stuaddress='�����人' and stuage=20
select * from exam  where exlab>=60 and exlab<=80 order by exlab desc
select * from exam where exlab between 60 and 80 order by exlab desc