## 本节目标

1. 熟悉SQL Server表关系一对一
2. 熟悉SQL Server表关系一对多
3. 熟悉SQL Server表关系多对多

## 数据准备

之前都是学生管理系统，现在来一个短视频的，比如：

```sql
-- 创建TikTok数据库
create database TikTok;
go
-- 使用TikTok数据库
use TikTok;
go
```

## 一对一

从表关系上来看，一对一是表1和表2之间的记录，是一条记录对应一条记录的关系。

**示例1**，比如短视频应用内有很多的用户信息需要存储：

```sql
-- 创建用户信息表
create table UserInfo(
	UserId int primary key identity(1, 1),
	Nickname nvarchar(20) not null
);
```

短视频应用有一个开播业务，需要用户进行实名认证：

```sql
-- 创建身份认证信息表
create table UserIDCardInfo(
	UserId int primary key,
	RealName nvarchar(10) not null,
	IDCardNumber nvarchar(20) not null
);
```

其中UserIDCardInfo.UserId字段是UserInfo.UserId的外键。

```sql
-- 身份认证信息表设置UserId外键到UserInfo表的UserId
alter table UserIDCardInfo add constraint FK_UserIDCardInfo_UserId foreign key (UserId) references UserInfo(UserId);
```

插入一些测试数据：

```sql
-- 用户信息表插入多条数据记录
insert into UserInfo (Nickname) values ('小绿茶');
insert into UserInfo (Nickname) values ('姗姗YOYO');
insert into UserInfo (Nickname) values ('七七情感聆听');
insert into UserInfo (Nickname) values ('追剧小朋友');
insert into UserInfo (Nickname) values ('桥天之娇子');
select * from UserInfo;

-- 身份认证信息表，插入多条记录
insert into UserIDCardInfo (UserId, RealName, IDCardNumber) values (1, '江美美', '110101199003072578');
insert into UserIDCardInfo (UserId, RealName, IDCardNumber) values (2, '陈静静', '110101199003078980');
select * from UserIDCardInfo;
```

从这边可以看出：UserInfo和UserIDCardInfo是一一对应的关系。

**一对一的常用场景**：

1. 垂直拆分，分库分表，加快性能。
2. 由于业务分工协作。
3. 后续数据量太大，加字段无法增加上去。

一对一常见的设计技巧：**两个表的主键字段相同，其中子集表的主键关联到父集表的主键。**。

## 一对多

从表关系上来看，一对多是表1和表2之间的记录，是一条记录对多条记录的关系。

**示例1**，在短视频应用里，一个用户可以发多个视频，一个视频只能属于一个用户。我们创建一个视频表来存储视频信息。

```sql
-- 创建短视频表 
create table Video(
	VideoId int primary key identity(1, 1),
	UserId int not null,
	VideoDesc nvarchar(255) not null,
	CoverPicUrl nvarchar(512) not null,
	VideoUrl nvarchar(512) not null
);
go
-- 插入短视频信息数据
insert into Video (UserId, VideoDesc, CoverPicUrl, VideoUrl) 
	values (1, '国宝好萌', 'https://mvimg10.meitudata.com/5efc2e4d292b4bgo29itdj2058.jpg!thumb480', 
	'https://mvvideo11.meitudata.com/5efc2e4b1f714j4jpv8o8t8888_H264_1_193afef3323948_H264_4_19a9e05b5e300b.mp4'),
	(2, '涨本事了啊', 'https://mvimg11.meitudata.com/5f150a2b66cf5149t7bf93868.jpg!thumb480', 
	'https://mvvideo11.meitudata.com/5f150a2b9eb8efwdro50kl2863_H264_4_1f0509d88b6dbf.mp4'),
	(1, '#汪星人#一边看电视一边撸狗', 'https://mvimg10.meitudata.com/5f291f1abdeabglzzgz4dl2100.jpg!thumb140', 
	'https://mvvideo11.meitudata.com/5f06925582bc81ftjm8bni3734_H264_4_1ba6be27ced62a.mp4');
go

select * from Video;
```

在这里，UserInfo 和 Video 就是一对多的关系。

**示例2**，一个视频可以有多个评论，一个评论只能对应一个视频：

```sql
-- 创建评论表
create table Comment(
	CommentId int primary key identity(1, 1), -- 评论id
	UserId int not null, -- 发表评论的用户id
	Content nvarchar(255) not null, -- 评论内容
	VideoId int not null, -- 视频id
	CreateAt datetime -- 发表时间
);
go
-- 评论表设置UserId外键到UserInfo表的UserId
alter table Comment add constraint FK_Comment_UserId foreign key (UserId) references UserInfo(UserId);
-- 评论表设置VideoId外键到Video表的VideoId
alter table Comment add constraint FK_Comment_VideoId foreign key (VideoId) references Video(VideoId);
go

-- 对视频1进行评论，插入若干条数据记录
insert into Comment (UserId, Content, VideoId, CreateAt) values 
	(3, '牙口真好', 1, '2020-07-24 21:18:00')
	,(4, '这是假胡萝卜吧？', 1, '2020-07-27 20:14:00')
	,(5, '可爱的不要不要的🤗🤗🤗', 1, '2020-07-09 22:02:00');
go
-- 对视频2进行评论，插入若干条数据记录
insert into Comment (UserId, Content, VideoId, CreateAt) values 
	(3, '哈哈哈哈', 2, '2020-07-20 12:14:00')
	,(4, '2', 2, '2020-07-20 15:01:00');
go
```

其实一对多的业务场景非常常见，常见的设计技巧：**就是在多的那个表设计一个字段关联到一那个表的主键**。

## 多对多

从表关系上来看，一对多就是表1到表2是1对多，表2到表1也是一对多。

**示例1**，一个用户可以收到多种礼物，一种礼物可以送给多个用户：

```sql
-- 创建礼物表
create table Gift(
	GiftId int primary key identity(1, 1) , -- 礼物id
	Name nvarchar(20) not null, -- 礼物名称
	Coin int not null, -- 礼物价值
);

insert into Gift (Name, Coin) values 
	('小心心', 1)
	,('直升机', 2999)
	,('浪漫马车', 28888);
select * from Gift;

-- 创建收礼表
create table GiftReceive(
	GiftReceiveId int primary key identity(1, 1), -- 自增id
	UserId int not null, -- 收礼的用户id
	GiftId int not null, -- 礼物id
    SendUserid int not null ,-- 送礼用户的Id
	CreateAt datetime -- 收到礼物的时间
);
go
alter table GiftReceive add constraint FK_GiftReceive_UserId foreign key (UserId) references UserInfo(UserId);
alter table GiftReceive add constraint FK_GiftReceive_GiftId foreign key (GiftId) references Gift(GiftId);
go

-- 用户1收到的礼物
insert into GiftReceive (UserId, GiftId, CreateAt) values 
	(1, 1, '2020-07-28 19:00:55')
	,(1, 1, '2020-07-28 20:00:04')
	,(1, 2, '2020-07-28 20:01:36')
	,(1, 3, '2020-07-28 20:02:20');
-- 用户2收到的礼物
insert into GiftReceive (UserId, GiftId, CreateAt) values 
	(2, 1, '2020-07-29 19:01:55')
	,(2, 2, '2020-07-29 20:08:04')
	,(2, 2, '2020-07-29 20:18:36')
	,(2, 3, '2020-07-29 20:28:20');
go
```

**示例2**，用户之间可以互相关注：

```sql
create table Follow(
	FollowId int primary key identity(1, 1),
	UserId int not null, -- 发起的用户id
	FollowUserId int not null, -- 被关注的用户id
	CreateAt smalldatetime not null, -- 关注时间
);

select * from UserInfo;
select * from Follow;
-- 用户1关注了用户2和用户3
insert into Follow (UserId, FollowUserId, CreateAt) values (1, 2, GETDATE());
insert into Follow (UserId, FollowUserId, CreateAt) values (1, 3, GETDATE());
-- 用户2关注了用户1和用户4
insert into Follow (UserId, FollowUserId, CreateAt) values (2, 1, GETDATE());
insert into Follow (UserId, FollowUserId, CreateAt) values (2, 4, GETDATE());
```

这边用户和用户之间可以互粉，从表关系上来看，UserInfo和UserInfo表形成了多对多的关系。

多对多常见的设计技巧：**就是新增加一个表，分别包含两个表的主键字段，记录关系**。

查询出用户1关注的用户，只需要查询出有互粉的即可：

```sql
select * from Follow as A inner join Follow as B 
	on A.UserId = B.FollowUserId 
	where A.UserId = 1 AND A.FollowUserId = B.UserId
```

增加是否互粉字段：

```sql
select A.*, 
	case when A.FollowUserId = B.UserId then '是'
		else '否'
		end as '是否互粉'
	from Follow as A inner join Follow as B 
	on A.UserId = B.FollowUserId 
	where A.UserId = 1
```

## 本节总结

1. 一对一：一对一是表1和表2之间的记录，是一条记录对应一条记录的关系。两个表的主键字段相同，其中子集表的主键关联到父集表的主键。
2. 一对多：一对多是表1和表2之间的记录，是一条记录对多条记录的关系。就是在多的那个表设计一个字段关联到一那个表的主键。
3. 多对多：表1到表2是1对多，表2到表1也是一对多。就是新增加一个表，分别包含两个表的主键字段，记录关系。
