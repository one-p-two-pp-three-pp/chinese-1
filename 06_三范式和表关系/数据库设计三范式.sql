--------------------------------------------------------------------------------------------------
--数据库三范式：数据库的设计范式是数据库设计所需要满足的规范，满足这些规范的数据库是结构合理和减少冗余的。
--				同时，不会发生插入（insert）、删除（delete）和更新（update）操作异常。
--1.第一范式（1NF）：列不可再分；每一列属性都是不可再分的属性值，确保每一列的原子性。
--2.第二范式（2NF）：属性完全依赖于主键；即实体唯一性
--3.第三范式（3NF）：消除了非主属性对于主属性的传递依赖。属性不依赖于其它非主属性，属性直接依赖于主键

--总结：三大范式只是一般设计数据库的基本理念，可以建立冗余较小、结构合理的数据库。
--如果有特殊情况，当然要特殊对待，数据库设计最重要的是看需求跟性能，需求>性能>表结构。所以不能一味的去追求范式建立数据库。

------------------------------------------------------------------------------------------------------


--创建数据库
create database TestDB
go

--使用TestDB数据库
use TestDB 
go

----------------------------------------------------------------------------------------------------------
----1.第一范式（1NF）：列不可再分；每一列属性都是不可再分的属性值，确保每一列的原子性。
--				 意义：数据存储结构更加合理，有利于数据的使用，如便于查询和对数据的统计分析。
--				 例如：如下两张学生信息表结构设计，在需要对学生信息按省份进行分类统计时，明显符合第一范式的表2更方便数据的处理
----------------------------------------------------------------------------------------------------------
--创建学生信息表1（列不具有原子性）   --不符合第一范式
create table StuInfo1
(
	StuId int identity(1,1) primary key,	--主键，学号，标识列
	StuName nvarchar(10) not null,		--姓名，非空

	StuContact nvarchar(200),--联系方式：包含手机号，邮箱等；不符合第一范式，列可以在拆分
	StuAddress nvarchar(200) --地址:包含省份，市，区县，街道等 ；不符合第一范式，列可以在拆分
)
go

--创建学生信息表2（将表StuInfo1的列进一步拆分为不可再拆分的列）   --符合第一范式
create table StuInfo2
(
	StuId int identity(1,1) primary key,	--主键，学号，标识列
	StuName nvarchar(10) not null,		--姓名，非空

	--将表StuInfo1的联系方式字段拆分成多列：手机号，微信，QQ，邮箱
	Phone nvarchar(11) check(len(Phone)=11) unique,--手机号，限制11位,唯一不重复
	WeiXin nvarchar(50),--微信
	QQ nvarchar(20),--QQ 
	Email nvarchar(10) unique,--邮箱

	--将表StuInfo1的地址字段拆分成多列：省份，城市，区县，街道，详细地址
	Province nvarchar(20),--省份
	City nvarchar(20),--城市
	County nvarchar(20),--区县
	Street nvarchar(20),--街道
	DetailedAddress nvarchar(200) --详细地址

)
go


---------------------------------------------------------------------------------------
----2.第二范式（2NF）：属性完全依赖于主键；即实体唯一性。
--					   第二范式（2NF）是在第一范式（1NF）的基础上建立起来的，即满足第二范式（2NF）必须先满足第一范式（1NF）。
--					   第二范式（2NF）要求数据库表中的每个实例或行必须可以被惟一地区分。为实现区分通常需要为表加上一个列，以存储各个实例的惟一标识。这个惟一属性列被称为主键
--				  意义：减小数据的冗余，便于数据的维护。	
---------------------------------------------------------------------------------------
--学生课程信息表包含学生和课程信息（不符合二范式，部分依赖主键，每一行记录包含了学生和课程两个实体，数据存在大量的重复）
create table StuInfo
(
	StuId int identity(1,1) ,	--学号，标识列
	StuName nvarchar(10) not null,		--姓名，非空

	--选修课程信息
	CourseName nvarchar(50) unique not null ,--课程名称，非空，唯一不重复
	CourseCredit int default(1) check(CourseCredit between 1 and 5), --学分，默认值为1，取值范围1-5

	primary key(StuId,CourseName)	--联合主键（学号和课程名称一起组合才能唯一识别一条记录）
)
go


--将上面的表格拆分成三张表，使每张表的每一行记录只有一个实体
--创建学生信息表
create table StuInfo
(
	StuId int identity(1,1) primary key,	--主键，学号，标识列
	ClassId int references ClassInfo(ClassId) on delete set null, --所属班级编号，外键关联班级表的班级编号
	StuName nvarchar(10) not null,		--姓名，非空
	
)
go

--创建成绩表（中间表）
create table Scores
(
	StuId int references StuInfo(StuId),--学号，外键关联学生信息表的学号
	CourseId int references CourseInfo(CourseId),--课程编号，外键关联课程信息表的课程编号
	Score int default(0),	--成绩,默认为0

	primary key(StuId,CourseId)	--联合主键（学号和课程编号一起组合才能唯一识别一条记录）
)
go

--创建课程信息表
create table CourseInfo
(
	CourseId int identity(1,1) primary key,	--课程编号，主键，标识列
	CourseName nvarchar(50) unique not null ,--课程名称，非空，唯一不重复
	CourseCredit int default(1) check(CourseCredit between 1 and 5) --学分，默认值为1，取值范围1-5
)
go







-----------------------------------------------------------------------------------------
--3.第三范式（3NF）：非主键外的所有字段必须互不依赖；属性不依赖于其它非主属性,属性直接依赖于主键
--					 数据不能存在传递关系，即每个属性都跟主键有直接关系而不是间接关系。
--			   意义：减小数据的冗余，便于数据的维护。
--			   例如：学号->班级->班级简介  属性之间含有这样的关系，是不符合第三范式的。
------------------------------------------------------------------------------------------
--创建学生信息表（学号->班级名称->班级简介，班级简介依赖班级名称，班级名称依赖学号，所以班级简介间接依赖学号主键）
create table StuInfo
(
	StuId int identity(1,1) primary key,	--主键，学号，标识列
	ClassId int references ClassInfo(ClassId) on delete set null, --所属班级编号，外键关联班级表的班级编号
	StuName nvarchar(10) not null,		--姓名，非空

	ClassName nvarchar(20) not null,			--班级名称，非空
	ClassDec nvarchar(max) --班级描述
)
go



--将表拆分成两张表，使所以的非主属性直接依赖于主属性，不存在传递依赖
--创建班级表
create table ClassInfo
(
	ClassId int identity(1,1) primary key,  --主键，班级编号，标识列
	ClassName nvarchar(20) not null,			--班级名称，非空
	ClassDec nvarchar(max) --班级描述
)
go

--创建学生信息表
create table StuInfo
(
	StuId int identity(1,1) primary key,	--主键，学号，标识列
	ClassId int references ClassInfo(ClassId) on delete set null, --所属班级编号，外键关联班级表的班级编号
	StuName nvarchar(10) not null,		--姓名，非空
)
go